/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
      },
      colors: {
        primary: '#0F172A',
        secondary: '#64748B',
        select_option: "#F1F5F9",

      },
      fontSize: {
        'sm': '13px',
        'base': '15px',
        'lg': '32px',
      },
      margin: {
        '5px': '5px',
        '29px': '29px',
      },
      lineHeight: {
        'relaxed': '22.5px',
        'loose': '19.5px',
      }
    },
  },
  plugins: [],
}
