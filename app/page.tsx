import React from 'react';
import dynamic from 'next/dynamic';
import Table, { Game } from './Table';
import { data } from './data';

const Home: React.FC = () => {
  return (
    <div className="container mx-auto p-4">
      <h1 className="text-lg font-medium mb-6px text-primary">Best Free P2E NFT Games in 2022</h1>
      <p className="text-base font-normal mb-29px text-secondary">Are you looking for Games that Free-to-play? Here are the best F2P NFT games available.</p>
      <Table />
    </div>
  );
};

export default Home;
