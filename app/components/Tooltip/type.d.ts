import React from "react";


/**
 * Interface for the Tooltip component.
 * 
 * @property text: React.ReactNode (optional) - The text content to display in the tooltip.
 * @property children: React.ReactNode (required) - The children content wrapped inside the Tooltip component.
 * @property style: React.CSSProperties (optional) - Additional styles to be applied to the Tooltip component.
 */

export interface ITooltip{
    text?: React.ReactNode
    children: React.ReactNode
    style?: React.CSSProperties;
}