import React, { useState } from 'react';
import { ITooltip } from './type';

const Tooltip :React.FC<ITooltip> = ({ text, children, style  }) => {
  const [isTooltipVisible, setIsTooltipVisible] = useState(false);

  const handleMouseEnter = () => {
    setIsTooltipVisible(true);
  };

  const handleMouseLeave = () => {
    setIsTooltipVisible(false);
  };

  return (
    <div className="relative inline-block">
      <div
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        {children}
      </div>
      {isTooltipVisible && (
        <div style={style} className="absolute bottom-full left-1/2 transform -translate-x-1/2 mt-2 bg-slate-200 text-white text-sm rounded-md px-2 py-1">
          {text}
        </div>
      )}
    </div>
  );
};

export default Tooltip;
