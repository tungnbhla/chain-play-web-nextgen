"use client"; // This is a client component
import React, { useEffect, useState } from "react";
import { data } from "./data";
import Tooltip from "./components/Tooltip";

export interface Game {
  Code: string;
  Name: string;
  ImageUrl: string;
  Symbol: string;
  BlockChains: { Name: string; Code: string; ExtendValue: string }[];
  Genres: { Name: string; Code: string }[];
  Platforms: { Name: string; Code: string }[];
  Price: number;
}

const Table: React.FC = () => {
  const [filter, setFilter] = useState("all");
  const [search, setSearch] = useState("");
  const [blockChains, setBlockChains] = useState<string[]>([]);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 14;

  useEffect(() => {
    const uniqueBlockChains = Array.from(
      new Set(
        data.flatMap((game) =>
          game.BlockChains.map((blockChain) => blockChain.Name)
        )
      )
    );
    setBlockChains(uniqueBlockChains);
  }, []);

  const handleFilterChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setFilter(e.target.value);
  };

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  const filteredData = data.filter((game) => {
    if (filter === "all") {
      return true;
    } else {
      return game.BlockChains.some((blockChain) => blockChain.Name === filter);
    }
  });

  const searchedData = filteredData.filter(
    (game) =>
      game.Name.toLowerCase().includes(search.toLowerCase()) ||
      game.Symbol.toLowerCase().includes(search.toLowerCase())
  );

  const totalPages = Math.ceil(searchedData.length / itemsPerPage);
  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentItems = searchedData.slice(startIndex, endIndex);

  console.log('totalPages :>> ', totalPages);

  return (
    <div className="overflow-x-auto">
      <div className="flex items-center justify-between mb-4">
        <select
          className="px-3 py-1 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
          value={filter}
          onChange={handleFilterChange}
        >
          <option value="all">All</option>
          {blockChains.map((blockChain) => (
            <option key={blockChain} value={blockChain}>
              {blockChain}
            </option>
          ))}
        </select>
        <input
          className="px-3 py-1 border border-gray-300 rounded-md focus:outline-none focus:ring-2 focus:ring-blue-500"
          type="text"
          placeholder="Search"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>
      <table className="min-w-full">
        <thead>
          <tr>
            <th className="px-6 py-3 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              #
            </th>
            <th className="px-6 py-3 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              NAME
            </th>
            <th className="px-6 py-3 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              GENRES
            </th>
            <th className="px-6 py-3 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
              Platform
            </th>
          </tr>
        </thead>
        <tbody>
          {currentItems.map((game, index) => (
            <tr key={game.Code}>
              <td className="px-6 py-4 whitespace-nowrap">
                {(currentPage - 1) * itemsPerPage + index + 1}
              </td>
              <td className="px-6 py-4 whitespace-nowrap">
                <div className="flex items-center">
                  <div className="flex-shrink-0 w-10 h-10">
                    <img
                      className="w-full h-full rounded-full"
                      src={game.ImageUrl}
                      alt={game.Name}
                    />
                  </div>
                  <div className="ml-4">
                    <div className="inline-flex align-bottom text-base font-medium items-center mb-1 leading-relaxed">
                      <p className="text-primary">{game.Name}</p>{" "}
                      <p className="text-secondary font-normal text-sm ml-1">
                        {game.Symbol.toUpperCase()}
                      </p>
                    </div>
                    <div className="flex">
                      {game.BlockChains.map((e, index) => {
                        return (
                          <div
                            className="inline-flex align-bottom items-center text-secondary leading-loose"
                            key={e.Name}
                          >
                            {game.BlockChains.length > 1 ? (
                              <Tooltip text={e.Name}>
                                <img
                                  className="w-5 h-5 rounded-full mr-1"
                                  src={e.ExtendValue || ""}
                                  alt={e.Name}
                                />
                              </Tooltip>
                            ) : (
                              <>
                                <img
                                  className="w-5 h-5 rounded-full mr-1"
                                  src={e.ExtendValue || ""}
                                  alt={e.Name}
                                />
                              </>
                            )}
                            <p className="text-sm">{game.BlockChains.length > 1 ? "" : e.Name}</p>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
              </td>
              <td className="px-6 py-4 whitespace-nowrap text-primary text-base">
                {game.Genres.map((genre) => genre.Name).join(" | ")}
              </td>
              <td className="px-6 py-4 text-primary whitespace-nowrap text-primary text-base">
                {game.Price}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="flex justify-center mt-4">
        {currentPage > 4 && (
          <button
            className="mx-1 px-2 py-1 rounded-md bg-gray-200"
            onClick={() => handlePageChange(1)}
          >
            1
          </button>
        )}
        {currentPage > 4 && <span className="mx-1">...</span>}
        {Array.from({ length: totalPages }, (_, index) => index + 1).map(
          (page) => {
            if (
              (page === 1 && currentPage <= 4) ||
              (page === currentPage &&
                currentPage >= 5 &&
                currentPage <= totalPages - 4) ||
              (page >= currentPage - 1 && page <= currentPage + 1) ||
              (page === totalPages && currentPage >= totalPages - 3)
            ) {
              return (
                <button
                  key={page}
                  className={`mx-1 px-2 py-1 rounded-md ${
                    page === currentPage
                      ? "bg-blue-500 text-white"
                      : "bg-gray-200"
                  }`}
                  onClick={() => handlePageChange(page)}
                >
                  {page}
                </button>
              );
            } else if (
              (page === totalPages - 1 && currentPage <= totalPages - 3) ||
              (page === totalPages && currentPage === totalPages - 2)
            ) {
              return (
                <span key={page} className="mx-1">
                  ...
                </span>
              );
            }
            return null;
          }
        )}
        {currentPage < totalPages && totalPages - currentPage > 3 ? (
          <button
            className="mx-1 px-2 py-1 rounded-md bg-gray-200"
            onClick={() => handlePageChange(totalPages)}
          >
            {totalPages}
          </button>
        ) : null}
      </div>
    </div>
  );
};

export default Table;
